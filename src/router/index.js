import Vue from 'vue'
import Router from 'vue-router'
import Dollar from '@/components/Dollar'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Dollar',
      component: Dollar
    }
  ]
})
