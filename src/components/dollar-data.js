export var dollar = {
  cash100: {
    src: 'static/images/100.jpg',
    alt: 'cash 100 dollar',
    fluid: true,
    rounded: true
  },
  cash50: {
    src: 'static/images/50.jpg',
    alt: 'cash 50 dollar',
    fluid: true,
    rounded: true
  },
  cash20: {
    src: 'static/images/20.jpg',
    alt: 'cash 20 dollar',
    fluid: true,
    rounded: true
  },
  cash10: {
    src: 'static/images/10.jpg',
    alt: 'cash 10 dollar',
    fluid: true,
    rounded: true
  },
  cash5: {
    src: 'static/images/5.jpg',
    alt: 'cash 5 dollar',
    fluid: true,
    rounded: true
  },
  cash1: {
    src: 'static/images/1.jpg',
    alt: 'cash 1 dollar',
    fluid: true,
    rounded: true
  },
  cent50: {
    src: 'static/images/c50.jpg',
    height: 60,
    alt: 'cash 50 cent'
  },
  cent25: {
    src: 'static/images/c25.jpg',
    height: 60,
    alt: 'cash 25 cent'
  },
  cent10: {
    src: 'static/images/c10.jpg',
    height: 60,
    alt: 'cash 10 cent'
  },
  cent5: {
    src: 'static/images/c5.jpg',
    height: 60,
    alt: 'cash 5 cent'
  },
  cent1: {
    src: 'static/images/c1.jpg',
    height: 60,
    alt: 'cash 1 dollar'
  }
}
